import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private input: string = '';
  private output: string = '';

  constructor(public snackBar: MatSnackBar) {}

  private main(input: string): void {
    try {
      if(!input) throw new Error('El input esta vacío!');

      let inputLines: Array<string> = input.split('\n');

      if(!parseInt(inputLines[0])) throw new Error('Por Favor indicar la cantidad de segmentos');
      if((parseInt(inputLines[0])<1) || (parseInt(inputLines[0])>50)) throw new Error('La cantidad de segmentos debe estar entre 1 y 50');

      let line: number = 0;
      let segments: number = parseInt(inputLines[line++]);

      for (let segment: number = 0; segment < segments; segment++) {
        if(!inputLines[line]) throw new Error('Faltan instrucciones!');

        let instructions: Array<number> = inputLines[line++].split(' ').map(Number).filter(Boolean);
        let length: number = instructions[0];
        let operations: number = instructions[1];

        if((length<1) || (length>100)) throw new Error('El tamaño del cubo debe estar entre 1 y 100');
        if((operations<1) || (operations>1000)) throw new Error('La cantidad de operaciones debe estar entre 1 y 1000');

        let grid: number[][][] = this.createCube(length);
        for (let i: number = 0; i < operations; i++) {
          let operation: string = inputLines[line++];
          this.execute(operation, grid);
        }
      }
    } catch (e) {
      this.output = '';
      this.snackBar.open(e, null,{
        extraClasses: ['error-snackbar'],
        duration: 2000
      });
    }
  }

  private createCube(length: number): number[][][] {
    let cube: number[][][] = [];
    for (let x: number = 0; x < length; x++) {
      cube[x] = [];
      for (let y: number = 0; y < length; y++) {
        cube[x][y] = [];
        for (let z: number = 0; z < length; z++) {
          cube[x][y][z] = 0;
        }
      }
    }
    return cube;
  }

  private execute(line: string, cube: number[][][]): void {
    if(!line) throw new Error('Hace falta instrucciones!');

    let operation: string[] = line.split(' ');
    let type: string = operation[0];
    let coordinations: number[] = [];

    let num = 0;
    for (let i: number = 1; i < operation.length; i++) {
      let position = parseInt(operation[i]);

      if(!position && operation[i]) throw new Error('Las coordenadas deben ser numeros positivos');

      coordinations[num++] = position;
    }

    if (type === 'UPDATE') {
      let N = cube.length;

      if((coordinations[0]<1) || (coordinations[0]>N)) throw new Error('El update debe cumplir la siguientes condicione: \n1 <= x <= ' + N);
      if((coordinations[1]<1) || (coordinations[1]>N)) throw new Error('El update debe cumplir la siguientes condicione: \n1 <= y <= ' + N);
      if((coordinations[2]<1) || (coordinations[2]>N)) throw new Error('El update debe cumplir la siguientes condicione: \n1 <= z <= ' + N);

      if((coordinations[3]<Math.pow(-10, 9)) || (coordinations[3]>Math.pow(10, 9))) throw new Error('El update debe cumplir la siguientes condicione: \n-10ˆ9 <= W <= 10ˆ9');

      this.update(coordinations, cube);
    } else if (type === 'QUERY') {
      let N = cube.length;

      if((!coordinations[0]) || (!coordinations[1]) || (!coordinations[2]) ||
         (!coordinations[3]) || (!coordinations[4]) || (!coordinations[5])) throw new Error('Hace falta coordenadas en la query');

      if((coordinations[0]<1) || (coordinations[0]>coordinations[3]) || (coordinations[3]>N)) throw new Error('La query debe cumplir la siguientes condicione: \n1 <= x1 <= x2 <= ' + N);
      if((coordinations[1]<1) || (coordinations[1]>coordinations[4]) || (coordinations[4]>N)) throw new Error('La query debe cumplir la siguientes condicione: \n1 <= y1 <= y2 <= ' + N);
      if((coordinations[2]<1) || (coordinations[2]>coordinations[5]) || (coordinations[5]>N)) throw new Error('La query debe cumplir la siguientes condicione: \n1 <= z1 <= z2 <= ' + N);

      this.query(coordinations, cube);
    } else {
      throw new Error('Tipo de operación inválida!');
    }
  }

  private update(values: number[], cube: number[][][]): void {
    cube[values[0]-1][values[1]-1][values[2]-1] = values[3];
  }

  private query(values: number[], cube: number[][][]): void {
    let suma: number = 0;

    for (let x: number = values[0]-1; x <= values[3]-1; x++) {
      for (let y: number = values[1]-1; y <= values[4]-1; y++) {
        for (let z: number = values[2]-1; z <= values[5]-1; z++) {
          suma += cube[x][y][z];
        }
      }
    }

    this.output += suma + '\n';
  }
}
