# CubeSummation

Cube Summation challenge in Angular from https://www.hackerrank.com/challenges/cube-summation

[![Screen_Shot_2018-01-05_at_2.55.18_PM.png](https://s10.postimg.org/n19kdatmx/Screen_Shot_2018-01-05_at_2.55.18_PM.png)](https://postimg.org/image/yqdk19klh/)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Input
```
2
4 5
UPDATE 2 2 2 4
QUERY 1 1 1 3 3 3
UPDATE 1 1 1 23
QUERY 2 2 2 4 4 4
QUERY 1 1 1 3 3 3
2 4
UPDATE 2 2 2 1
QUERY 1 1 1 1 1 1
QUERY 1 1 1 2 2 2
QUERY 2 2 2 2 2 2
```

### Output
```
4
4
27
0
1
1
```
### License
MIT. Copyright © [Andrés Ariza](http://andresariza.co)
